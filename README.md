# Peru Streamflow Prediction

Reproduce figures for associated paper "Leveraging multi-model season-ahead streamflow forecasts to trigger advanced flood preparedness in Peru"

## Installation

```bash
git clone https://gitlab.com/ckeating/peru_streamflow_prediction
```

## Usage

From the RStudio IDE:
1. run 1_NIPA_modified.Rmd
2. run 2_GloFAS_Forecast_Verification_UPDATED.Rmd

Figures are saved to the *output* directory

*Please note: To run, you will need Piura River and/or Marañón River streamflow data which are unfortunately not publicly accessible at this time. These datasets are from SENAMHI and may be made available upon request.*