# Author: Colin Keating
# Last Updated 2/18/2021
#
#
# Returns years that trigger early action from multi-model ensemble
#
multi_model_function <- function(is_amazon, is_maranon, is_piura, use_equal_sized_categories = FALSE, high_low_cutoff = 0.8, combination_method, trigger_threshold = 0.8) {
  # Load probabalistic forecasts from NIPA blended and GloFAS
  if (is_maranon) {
    load("multi_model_data/all_predicted_prob_maranon_MAM_1999_2017.RData")
    load("multi_model_data/all_predicted_det_maranon_MAM_1999_2017.RData")
    load("multi_model_data/obs_maranon_MAM_1999_2017.RData")
    load("multi_model_data/GloFAS_pred_maranon_MAM_1999_2017.RData")
    all_ensembles <- all_ensembles[1:25, ]
    start_year <- 1999
    end_year <- 2017
  }
  if (is_amazon) {
    load("multi_model_data/all_predicted_prob_amazon_MAM_1986_2017.RData")
    load("multi_model_data/all_predicted_det_amazon_MAM_1986_2017.RData")
    load("multi_model_data/obs_amazon_MAM_1986_2017.RData")
    load("multi_model_data/GloFAS_pred_amazon_MAM_1986_2017.RData")
    all_ensembles <- all_ensembles[1:25, 1:32]
    start_year <- 1986
    end_year <- 2017
  }
  if (is_piura) {
    load("multi_model_data/all_predicted_prob_piura_FMA_1982_2017.RData") # variable name "all_predicted_prob"
    load("multi_model_data/all_predicted_det_piura_FMA_1982_2017.RData") # variable name "all_predicted_det"
    load("multi_model_data/obs_piura_FMA_1982_2017.RData") # variable name "obs"
    load("multi_model_data/GloFAS_pred_piura_FMA_1981_2017.RData")
    all_ensembles <- all_ensembles[1:25, 1:36] # originally 1981-2017, but remove first year GloFAS starts in 1982
    start_year <- 1982
    end_year <- 2017
  }
  high_threshold <- quantile(obs, high_low_cutoff)


  # Methods to combine models - see JAWRA_Brazil_multimodel_SF.pdf
  # 1. linear combination of models
  # 2. least squares regression
  if (combination_method == 1) {
    # length of GloFas ensemble is 25 while length of NIPA blended ensemble is 1000; repeat GloFAS ensemble 40 times
    GloFAS <- all_ensembles[rep(seq_len(nrow(all_ensembles)), each = 40), ]
    # multi-model ensemble
    NIPA_Glofas <- rbind(GloFAS, all_predicted_prob)
  }
  if (combination_method == 2) {
    glofas_det <- colMeans(all_ensembles)
    model_data <- data.frame(
      "GloFAS" = glofas_det,
      "NIPA" = all_predicted_det,
      "observed" = obs
    )
    model <- lm(obs ~ ., data = model_data[, 1:2])
    nipa_weight <- model$coefficients[3]
    glofas_weight <- model$coefficients[2]


    n_glofas <- 1000 * glofas_weight / nipa_weight

    # function to round to the nearest 25
    mround <- function(x, base) {
      base * round(x / base)
    }
    n_glofas <- mround(n_glofas, 25)

    GloFAS <- all_ensembles[rep(seq_len(nrow(all_ensembles)), each = (n_glofas / 25)), ]
    # multi-model ensemble
    NIPA_Glofas <- rbind(GloFAS, all_predicted_prob)
  }

  #  CORRELATIONS
  # multi-model
  cor_pear <- cor.test(colMeans(NIPA_Glofas), obs, method = "pear")$estimate

  #  VERIFICATION
  #  put data in correct form for RPSS
  source("get_categorical_probs.R")
  both <- get_categorical_probs(
    all_ensembles = NIPA_Glofas,
    n_categories = 2,
    use_equal_sized_categories = FALSE,
    high_threshold = high_threshold,
    medium_threshold = medium_threshold,
    obs_sf = obs
  )
  observed_probs <- unlist(both[1])
  predicted_probs <- unlist(both[2])

  #  Compute RPSS
  source("compute_RPSS.R")
  rpss <- compute_RPSS(
    pred = predicted_probs,
    obs = observed_probs,
    use_equal_sized_categories = FALSE,
    n_categories = 2,
    climatology_high = (1 - high_low_cutoff),
    climatology_med = NA,
    climatology_low = high_low_cutoff,
    n_years = length(obs)
  )

  #  Contingency Table and Accuracy
  source("compute_accuracy_contingency.R")
  acc_cont <- compute_accuracy_contingency(obs_sf = obs, predicted_sf = colMeans(NIPA_Glofas), use_equal_sized_categories = FALSE, n_categories = 2, high_threshold = high_threshold, medium_threshold = NA)

  accuracy <- unlist(acc_cont[1])
  skill_matrix <- acc_cont[2]

  #  See when the RC EAP for flooding is triggered (default trigger threshold 0.8 or 80% chance)
  eightieth_val <- quantile(obs, trigger_threshold)
  n_years <- ncol(NIPA_Glofas)
  EAP_triggered_multi_MODEL <- rep(NA, n_years)
  members_above <- rep(NA, n_years)
  n_members <- nrow(NIPA_Glofas)
  # 25 ensembles so need 19 to be above threshold
  for (i in 1:n_years) {
    members_above[i] <- sum(NIPA_Glofas[, i] > eightieth_val, na.rm = TRUE)
    EAP_triggered_multi_MODEL[i] <- sum(NIPA_Glofas[, i] > eightieth_val, na.rm = TRUE) > ceiling(0.75 * n_members)
  }
  EAP_triggered_multi_MODEL_probs <- members_above / n_members
  return(list(EAP_triggered_multi_MODEL, accuracy, skill_matrix, rpss, cor_pear, EAP_triggered_multi_MODEL_probs))
}
