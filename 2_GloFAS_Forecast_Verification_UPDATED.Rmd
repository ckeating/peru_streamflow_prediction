---
title: "GloFAS Forecast Verification Computations"
---

## Purpose
Compute various forecast verification statistics for GloFAS seasonal prediction model

## References

### Required user inputs
```{r}
# SELECT timeseries to model
is_amazon <- FALSE
is_maranon <- TRUE
is_piura <- FALSE

draw_figs <- TRUE
# SELECT whether to bias-correct the ensemble means
use_bias_correction <- TRUE #(default = TRUE)
bc_method <- 1 # 1=mean bias correction (default and what we use in publication), 2=quantile mapping

use_two_category <- T
use_equal_sized_categories <- F
high_low_cutoff <- 0.8

medium_threshold <- 10 
```

### Setup / obtain observed sf data
```{r}
require(pls)
library(matlib)
set.seed (1000) # set for deterministic ensemble result when sampling error distribution

# PISCO 2.0 only goes thru 2016 so need to crop the streamflow vectors accordingly
# remove 2017 because precip doesn't have 2017 data (for purpose of comparison with statistical model output)

if(is_amazon){
  start_year <- 1986
  end_year <- 2017
  start_month <- 3 # MAM season
  end_month <- 5
  load("EAP_triggered_MODEL_amazon_NIPA_PCR.RData") # variable name is "EAP_triggered_MODEL"
  load("EAP_triggered_MODEL_probs_amazon_NIPA_PCR.RData") # variable name is "EAP_triggered_MODEL_probs"
  forecast_issue_month <- 3
}

if(is_maranon){
  start_year <- 1999
  end_year <- 2017
  start_month <- 3 # MAM season
  end_month <- 5
  load("EAP_triggered_MODEL_maranon_NIPA_PCR.RData") # variable name is "EAP_triggered_MODEL"
  load("EAP_triggered_MODEL_probs_maranon_NIPA_PCR.RData") # variable name is "EAP_triggered_MODEL_probs"
  forecast_issue_month <- 3
}

if(is_piura){
  start_year <- 1982
  end_year <- 2017
  start_month <- 2 # FMA season
  end_month <- 4
  load("EAP_triggered_MODEL_piura_NIPA_PCR.RData") # variable name is "EAP_triggered_MODEL"
  load("EAP_triggered_MODEL_probs_piura_NIPA_PCR.RData") # variable name is "EAP_triggered_MODEL_probs"
  forecast_issue_month <- 2
}
n <- end_year - start_year + 1
years <- c(start_year:end_year)

# get observed sf
if(is_amazon){
  all_monthly_means <- read.table("/Users/Colin/Box Sync/Research/data/Amazon_rivers/Amazon_rivers_edited/amazon_means_Rformat.txt")
}
if(is_maranon){
  all_monthly_means <- read.table("/Users/Colin/Box Sync/Research/data/Amazon_rivers/Amazon_rivers_edited/maranon_means_Rformat.txt")
}
if(is_piura){
  all_monthly_means <- read.table("/Users/Colin/Box Sync/Research/data/Amazon_rivers/Amazon_rivers_edited/piura_means_Rformat.txt")
}

# obtain observed streamflow for season
mean_by_month <- all_monthly_means[,(start_month+1):(end_month+1)]
obs_sf <- apply(mean_by_month,1,mean) 

if(is_piura){
  # remove 1971-1981
  obs_sf <- obs_sf[12:length(obs_sf)]
}

high_threshold <- quantile(obs_sf, high_low_cutoff)
```

### GLOFAS: Get ensemble predictions and plot against observed
```{r}
# see http://geog.uoregon.edu/bartlein/courses/geog490/week04-netCDF.html
library(ncdf4)
# Maranon at San Regis station 
# NOT LISTED
if(is_maranon){
  # The location of the Nauta station according to GloFAS map viewer is the same as the location returned by a google map search for San Regis.
  ncname <- "/Users/Colin/Box Sync/Research/data/GLOFAS/glofas2.0_seasonal_pointextract_discharge_for_Colin_Peru20190423/glofas2.0_seasonal_pointextract_discharge_for_G5424_Nauta_Peru20190423_19810101_20190401.nc"
}

# Amazon at Tamshiyacu station
if(is_amazon){
  ncname <- "/Users/Colin/Box Sync/Research/data/GLOFAS/glofas2.0_seasonal_pointextract_discharge_for_Colin_Peru20190423/glofas2.0_seasonal_pointextract_discharge_for_G5395_Tamshiyacu_Peru20190423_19810101_20190401.nc"
}

# Piura River
if(is_piura){
  ncname <- "/Users/Colin/Box Sync/Research/data/GLOFAS/glofas2.0_seasonal_pointextract_discharge_for_Colin_Peru20190423/glofas2.0_seasonal_pointextract_discharge_for_G5249_PuentePiura_Peru20190423_19810101_20190401.nc"
}
dname <- "dis"
ncin <- nc_open(ncname)
dis_array <- ncvar_get(ncin, dname)

# The first dimension (length 51) is # of ensemble members
# The second dimension (length 17) represents weekly flow 17 weeks/ 4 months out, see below)
# from fcprod2_compute_exceedance_probabilities.py:
# "This script, for one forecast (weekly-averaged river flow) for each week out to 17 weeks (4 months)"
# The third dimension (length 460) is number of months the forecast has been issued, since it started in 1981

# get the GloFAS ensemble predictions for discharge for season of interest (MAM or FMA)
nyears <- floor(dim(dis_array)[3]/12)
means_pred <- rep(0,nyears)
ensembles_pred <- matrix(NA,nrow = 51, ncol = nyears)

for (i in 0:(nyears)-1) {
  if(is_amazon|is_maranon){
    if(forecast_issue_month == 3){
      # Use weeks 1-13 to predict MAM sf with March 1st issue date (standard for CK statistical model)
      means_pred[i+1] <- mean(dis_array[,1:13,(12*i+3)],na.rm=TRUE)
      ensembles_pred[,(i+1)] <- rowMeans(dis_array[,1:13,(12*i+3)])
    }
    if(forecast_issue_month == 2){
      # Use weeks 5-17 to predict MAM sf with Feb 1st issue date
      means_pred[i+1] <- mean(dis_array[,5:17,(12*i+2)],na.rm=TRUE) 
      ensembles_pred[,(i+1)] <- rowMeans(dis_array[,5:17,(12*i+2)])
    }
  }
  if(is_piura){ # for predicting FMA discharge
    # Use weeks 5-17 to predict FMA sf with Jan 1st issue date
    if(forecast_issue_month == 1){
      means_pred[i+1] <- mean(dis_array[,5:17,(12*i+1)],na.rm=TRUE) 
      ensembles_pred[,(i+1)] <- rowMeans(dis_array[,5:17,(12*i+1)], na.rm = TRUE)
    }
    # Use weeks 1-13 to predict FMA sf with Feb 1st issue date
    if(forecast_issue_month == 2){
      means_pred[i+1] <- mean(dis_array[,1:13,(12*i+2)],na.rm=TRUE) 
      ensembles_pred[,(i+1)] <- rowMeans(dis_array[,1:13,(12*i+2)], na.rm = TRUE)
    }
  }
}

if(is_amazon){
  # trim to 1986-2017
  all_ensembles <- ensembles_pred[,6:(dim(ensembles_pred)[2]-1)]
}
if(is_maranon){
  # trim to 1999-2017
  all_ensembles <- ensembles_pred[,19:(dim(ensembles_pred)[2]-1)]
}
if(is_piura){
  # trim to 1982-2017
  all_ensembles <- ensembles_pred[,2:(dim(ensembles_pred)[2]-1)]
  #warning("TODO")
}

# bias-correct the emsemble predictions based on difference between mean observed and predicted value
if(use_bias_correction){
  
  # simple - adjust mean of ensemble by difference between ensemble and observed mean across all years
  if(bc_method==1){
    correction_factor <- mean(obs_sf) - mean(all_ensembles,na.rm=TRUE)
    all_ensembles <- all_ensembles + correction_factor
  }
  # quantile mapping
  # see https://cran.r-project.org/web/packages/qmap/qmap.pdf
  if(bc_method==2){
    library(qmap)
    ## call to fitQmapPTF and doQmapPTF
    qm1.fit <- fitQmapQUANT(obs_sf, all_ensembles[1:25,], method="QUANT", wett.day=TRUE)
    
    # stuff for plotting
    qqplot(obs_sf, colMeans(all_ensembles, na.rm=TRUE), main="before")
    abline(0,1)
    if(is_piura){
      x_seq <- seq(0, 1500, 50)
    }else{
      x_seq <- seq(0, 60000, 500)
    }
    obs_norm <- fitdist(obs_sf, "norm")
    pred_norm_before <- fitdist(c(all_ensembles[1:25,]), "norm")
    cdf_obs <- pnorm(x_seq, obs_norm$estimate[1], obs_norm$estimate[2])
    cdf_pred_before <- pnorm(x_seq, pred_norm_before$estimate[1], pred_norm_before$estimate[2])
    
    # correct each year:
    # do quantile mapping
    for(i in 1:ncol(all_ensembles)){
      all_ensembles[1:25,i] <- doQmap(all_ensembles[1:25,i],qm1.fit)
    }
  
    # plotting
    qqplot(obs_sf, colMeans(all_ensembles, na.rm=TRUE), main="after")
    abline(0,1)
    
    pred_norm_after <- fitdist(c(all_ensembles[1:25,]), "norm")
    cdf_pred_after <- pnorm(x_seq, pred_norm_after$estimate[1], pred_norm_after$estimate[2])
    
    plot(x_seq, cdf_obs)
    lines(x_seq, cdf_pred_before, col="red")
    lines(x_seq, cdf_pred_after, col="green")
    quant <- function(x,qstep=0.01){
      qq <- quantile(x,prob=seq(0,1,by=qstep))
    }
    plot(quant(all_ensembles[1:25,1]),
         quant(obs_sf[1]))
  }
}
```

# PLOTTING
```{r}
# GloFAS plot
# plot box+whisker predictions, observed sf
boxplot_color <- "#009E73"
ylabel <- expression(paste("Streamflow (",m^3,"/s)"))
par(mar = c(4, 5, 4, 4))
if(is_maranon){
  boxplot(all_ensembles, names=years[1:n], xlab='', ylab=ylabel, main='', outline=FALSE, col=boxplot_color, ylim=c(15000,35000))
}
if(is_amazon){
  boxplot(all_ensembles, names=years[1:n], xlab='', ylab=ylabel, main='', outline=FALSE, col=boxplot_color, ylim=c(30000,60000))
}
if(is_piura){
  boxplot(all_ensembles, names=years[1:n], xlab='', ylab=ylabel, main='GloFAS @ Piura', outline=FALSE, col=boxplot_color, ylim=c(0,1500))
}
lines(obs_sf[1:n], col='black')

# add 33% and 67% markers (delineating low, near-normal, and high flow conditions)
thresholds <- quantile(obs_sf[1:n], probs = c(0.333, 0.667, 0.8) )
lower_third <- rep(thresholds[1],n)
upper_third <- rep(thresholds[2],n)

eightieth <-  rep(thresholds[3],n)

#lines(seq(1,n), lower_third,col="black", lty=3)
#lines(seq(1,n), upper_third,col="black", lty=3)
lines(seq(1,n), eightieth,col="black", lty=3)


#legend(2010, 37000, legend=c("Observed", "Predicted"), col=c("black", "red"), lty=1:1, cex=0.8)


# save for use in multi-model ensemble with GloFAS
if(is_amazon){
  save(all_ensembles, file="multi_model_data/GloFAS_pred_amazon_MAM_1986_2017.RData")
}
if(is_maranon){
  save(all_ensembles, file="multi_model_data/GloFAS_pred_maranon_MAM_1999_2017.RData")
}
if(is_piura){
 save(all_ensembles, file="multi_model_data/GloFAS_pred_piura_FMA_1981_2017.RData")
}
```

Compare to RC EAP for Flooding (trigger is when 75% of members are above 80th percentile)

```{r}
EAP_triggered_GLOFAS <- rep(FALSE, n)
members_above <- rep(FALSE, n)
eightieth_val <- quantile(obs_sf[1:n], 0.8)

# 25 ensembles so need 19 to be above threshold
n_members <- 25
trigger_threshold <-  0.75 * n_members
for(i in 1:n){
  members_above[i] <- sum(all_ensembles[,i] > eightieth_val, na.rm=TRUE)
  EAP_triggered_GLOFAS[i] <- members_above[i] >= trigger_threshold
}
EAP_triggered_GLOFAS_probs <- members_above/n_members

# stores binary (did it trigger)
EAP_triggered_obs <- obs_sf > eightieth_val
# stores magnitude by which trigger surpassed 80th percentile
EAP_triggered_obs_magnitude <- obs_sf - eightieth_val

# get multi-model triggers:
source("/Users/Colin/Box Sync/Research/SST_Streamflow/multi_model_function.R")
multi_MODEL_equal_weight <- multi_model_function(is_amazon = is_amazon, is_maranon = is_maranon, is_piura = is_piura, combination_method = 1)
EAP_triggered_multi_MODEL_equal_weight <- unlist(multi_MODEL_equal_weight[1])
EAP_triggered_multi_MODEL_equal_weight_probs <- unlist(multi_MODEL_equal_weight[6])
multi_MODEL_least_squares <- multi_model_function(is_amazon = is_amazon, is_maranon = is_maranon, is_piura = is_piura, combination_method = 2)
EAP_triggered_multi_MODEL_least_squares <- unlist(multi_MODEL_least_squares[1])
EAP_triggered_multi_MODEL_least_squares_probs <- unlist(multi_MODEL_least_squares[6])  

only_true_EAP_obs <- ifelse(EAP_triggered_obs,2,NA)
NIPA_PCR_hits <- ifelse(EAP_triggered_MODEL&EAP_triggered_obs,3,NA)
NIPA_PCR_false_positives <- ifelse(EAP_triggered_MODEL&!EAP_triggered_obs,3,NA)
# if you drop confidence required to 50%, compute add'l false positives
NIPA_PCR_false_positives_50 <- ifelse(EAP_triggered_MODEL_probs>=0.5&!EAP_triggered_obs,3,NA)

GLOFAS_hits <- ifelse(EAP_triggered_GLOFAS&EAP_triggered_obs,4,NA)
GLOFAS_false_positives <- ifelse(EAP_triggered_GLOFAS&!EAP_triggered_obs,4,NA)
# if you drop confidence required to 50%, compute add'l false positives
GLOFAS_false_positives_50 <- ifelse(EAP_triggered_GLOFAS_probs>=0.5&!EAP_triggered_obs,4,NA)

# MM_equal_weight_hits <- ifelse(EAP_triggered_multi_MODEL_equal_weight&EAP_triggered_obs, 5, NA)
# MM_equal_weight_false_positives <- ifelse(EAP_triggered_multi_MODEL_equal_weight&!EAP_triggered_obs, 5, NA)
# # if you drop confidence required to 50%, compute add'l false positives
# MM_equal_weight_false_positives_50 <- ifelse(EAP_triggered_multi_MODEL_equal_weight_probs>=0.5&!EAP_triggered_obs,5,NA)

# UPDATE: set to 5 instead of 6
MM_least_squares_hits <- ifelse(EAP_triggered_multi_MODEL_least_squares&EAP_triggered_obs, 5, NA)
MM_least_squares_false_positives <- ifelse(EAP_triggered_multi_MODEL_least_squares&!EAP_triggered_obs, 5, NA)
# if you drop confidence required to 50%, compute add'l false positives
MM_least_squares_false_positives_50 <- ifelse(EAP_triggered_multi_MODEL_least_squares_probs>=0.5&!EAP_triggered_obs,5,NA)
```

View results:
```{r}
res <- data.frame("yr" = c(1999:2017), 
                  "statistical_prob" = round(EAP_triggered_MODEL_probs,2),
                  "glofas_prob" = EAP_triggered_GLOFAS_probs,
                  "multi-model_LS_prob" = round(EAP_triggered_multi_MODEL_least_squares_probs,2),
                  "actual_trigger" = EAP_triggered_obs)

View(res)
```

create and save plot
```{r}
if(draw_figs){
  #leg_text <- c("Ensemble\nL.S.", "Ensemble\nE.W.", "GloFAS", "NIPA/PCR", "Observed", "" , "MAM\nStreamflow\nRelative to\nThreshold")
  #leg_text <- c("Multi-model", "GloFAS", "Statistical", "Observed", "" , "MAM\nStreamflow\nRelative to\nThreshold\n(m^3*s)")
  last_label <- "Streamflow\nRelative to\nThreshold"
  leg_text <- c("Multi-model", "GloFAS", "Statistical", "Observed", "" , last_label)

  if(is_amazon){
    png("/Users/Colin/Box Sync/Research/manuscripts/Paper images and tables/EAP_triggering_amz_autosaved.png", width=3000, height=2000, pointsize=72)
    streamflow_thresh_cex <- 0.4
  }
  if(is_maranon){
    png("/Users/Colin/Box Sync/Research/manuscripts/Paper images and tables/EAP_triggering_mar_autosaved.png", width=3000, height=2000, pointsize=72)
    streamflow_thresh_cex <- 0.45
  }
  if(is_piura){
    png("/Users/Colin/Box Sync/Research/manuscripts/Paper images and tables/EAP_triggering_piura_autosaved.png", width=3000, height=2000, pointsize=72)
    streamflow_thresh_cex <- 0.35
  }
  par(lwd = 5, mar=c(6,5,4,2))
  # colorblind-friendly palette from http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/#a-colorblind-friendly-palette
  #cbbPalette <- c("#E69F00", "#56B4E9", "#009E73", "#D55E00", "#000000")
  #cbbPalette <- c("#E69F00", "#4DA1D1", "#009E73", "#D55E00", "#000000") # mod with darker blue
  cbbPalette <- c("#4DA1D1", "#009E73", "#D55E00", "#000000") # mod with darker blue & remove yellow

  #cbbPalette_lightened <- c("#ffd373", "#aad9f3", "#B2E1D5", "#ffac6a", "#000000") 
  cbbPalette_lightened <- c("#aad9f3", "#B2E1D5", "#ffac6a", "#000000") # remove yellow

  point_size <- 4
  
  # add normalized streamflow relative to trigger threshold:
  colorscheme <- ifelse(EAP_triggered_obs_magnitude<0,"grey", "black")
  bar_heights <- EAP_triggered_obs_magnitude/max(EAP_triggered_obs_magnitude)
  EAP_plot <- barplot(bar_heights, 
                        col = colorscheme, 
                        ylim=c(0,6), # changes height of plot
                        xlab="", ylab="", 
                        yaxt='n'
                        )
  # adds streamflow labels to barplot
  text_heights <- ifelse(bar_heights>0, bar_heights+0.1, 0.1)
  text(EAP_plot, 
       text_heights,
       labels=as.character(round(EAP_triggered_obs_magnitude,digits = 0)),
       cex=streamflow_thresh_cex)
  
  axis(3, at=EAP_plot, labels=c(start_year:end_year), cex.axis=0.7)
  
  #plot(years[1:n],only_true_EAP_obs,cex=point_size, pch=19, ylim=c(0,6), xlim=c(1970,2020), xlab="", ylab="", yaxt='n')
  
  # add true observed triggers:
  points(x=EAP_plot, only_true_EAP_obs, cex=point_size, pch=19, col="black")
  
  # add all trigger probabilities NIPA/PCR:
  points(x=EAP_plot, rep(3,n), cex=point_size*EAP_triggered_MODEL_probs, pch=19, col="grey")
  points(x=EAP_plot, rep(3,n)*ifelse(EAP_triggered_MODEL_probs>=0.5,1,NA), cex=point_size*EAP_triggered_MODEL_probs, pch=19, col=cbbPalette_lightened[3]) # for 50%
  points(x=EAP_plot, NIPA_PCR_hits,cex=point_size*EAP_triggered_MODEL_probs, pch=19, col=cbbPalette[3])
  points(x=EAP_plot, NIPA_PCR_false_positives_50,cex=point_size*EAP_triggered_MODEL_probs, pch=21, col=cbbPalette_lightened[3], lwd=10, bg="white")
  points(x=EAP_plot, NIPA_PCR_false_positives,cex=point_size*EAP_triggered_MODEL_probs, pch=21, col=cbbPalette[3], lwd=10, bg="white")
  
  # add all trigger probabilities Glofas:
  points(x=EAP_plot, rep(4,n), cex=point_size*EAP_triggered_GLOFAS_probs, pch=19, col="grey")
  points(x=EAP_plot, rep(4,n)*ifelse(EAP_triggered_GLOFAS_probs>=0.5,1,NA), cex=point_size*EAP_triggered_GLOFAS_probs, pch=19, col=cbbPalette_lightened[2]) # for 50%
  points(x=EAP_plot,GLOFAS_hits,cex=point_size*EAP_triggered_GLOFAS_probs, pch=19, col=cbbPalette[2])
  points(x=EAP_plot,GLOFAS_false_positives_50,cex=point_size*EAP_triggered_GLOFAS_probs, pch=21, col=cbbPalette_lightened[2], lwd=10, bg="white")
  points(x=EAP_plot,GLOFAS_false_positives,cex=point_size*EAP_triggered_GLOFAS_probs, pch=21, col=cbbPalette[2], lwd=10, bg="white")
  
  # add all trigger probabilities ensemble equal weight:
  # points(x=EAP_plot, rep(5,n), cex=point_size*EAP_triggered_multi_MODEL_equal_weight_probs, pch=19, col="grey")
  # points(x=EAP_plot, rep(5,n)*ifelse(EAP_triggered_multi_MODEL_equal_weight_probs>=0.5,1,NA), cex=point_size*EAP_triggered_multi_MODEL_equal_weight_probs, pch=19, col=cbbPalette_lightened[2]) # for 50%
  # points(x=EAP_plot,MM_equal_weight_hits,cex=point_size*EAP_triggered_multi_MODEL_equal_weight_probs, pch=19, col=cbbPalette[2])
  # points(x=EAP_plot,MM_equal_weight_false_positives_50,cex=point_size*EAP_triggered_multi_MODEL_equal_weight_probs, pch=21, col=cbbPalette_lightened[2], lwd=10, bg="white")
  # points(x=EAP_plot,MM_equal_weight_false_positives,cex=point_size*EAP_triggered_multi_MODEL_equal_weight_probs, pch=21, col=cbbPalette[2], lwd=10, bg="white")
  
  # add all trigger probabilities ensemble least squares:
  # update: lower to row y=5
  points(x=EAP_plot, rep(5,n), cex=point_size*EAP_triggered_multi_MODEL_least_squares_probs, pch=19, col="grey")
  points(x=EAP_plot, rep(5,n)*ifelse(EAP_triggered_multi_MODEL_least_squares_probs>=0.5,1,NA), cex=point_size*EAP_triggered_multi_MODEL_least_squares_probs, pch=19, col=cbbPalette_lightened[1]) # for 50%
  points(x=EAP_plot,MM_least_squares_hits,cex=point_size*EAP_triggered_multi_MODEL_least_squares_probs, pch=19, col=cbbPalette[1])
  points(x=EAP_plot,MM_least_squares_false_positives_50,cex=point_size*EAP_triggered_multi_MODEL_least_squares_probs, pch=21, col=cbbPalette_lightened[1], lwd=10, bg="white")
  points(x=EAP_plot,MM_least_squares_false_positives,cex=point_size*EAP_triggered_multi_MODEL_least_squares_probs, pch=21, col=cbbPalette[1], lwd=10, bg="white")
 
  #  x values are set according to EAP_plot
  if (is_maranon){
    leg_x_pos <- 0.5
    leg_y_pos <- 1
    x_interspacing <- c(2,1.75,1.3,0.9)
    text_width <- c(0,1.5,1.5,1.5)
  }
  if (is_piura){
    leg_x_pos <- 24.5
    leg_y_pos <- 1
    x_interspacing <- c(2,1.75,1.5,1.1)
    text_width <- c(0,3,3,3)
  }
  if (is_amazon){
    leg_x_pos <- 1.5
    leg_y_pos <- 1
    x_interspacing <- c(2.5,2,1.5,1)
    text_width <- c(0,1.75,1.5,1.5)
  }
    
  cex_size <- point_size*c(1,0.75,0.5,0.25)
  
  legend(
    x=leg_x_pos,
    y=leg_y_pos,
    legend=c("100%", "75%", "50%", "25%"), 
    bty="n",
    pch=1,
    cex = 0.5,
    pt.cex = cex_size,
    x.intersp = x_interspacing,
    text.width = text_width,
    horiz=T
  )
  
  box(lwd=5)
  par(lwd=4) 
  
  par(las=2)
  # now draw the first axis
  #axis(2, at=6:0, labels=leg_text, cex.axis=0.7)
  axis(2, at=5:0, labels=leg_text, cex.axis=0.7)
  
  dev.off()
}


# for debugging
years_triggers <- data.frame("year" = years[1:n],
                             "observed_triggers" = only_true_EAP_obs,
                             "NIPA/PCR" = EAP_triggered_MODEL,
                             "GLOFAS" = EAP_triggered_GLOFAS,
                             "multi-model-least_sq" = EAP_triggered_multi_MODEL_least_squares,
                             "multi-model-equal_wt" = EAP_triggered_multi_MODEL_equal_weight)
```

## TS vs trigger threshold plot
for every trigger threshold %, calculate TS for each model

TODO: make additional plots for POD and FAR to create a grid
```{r}
# Calculations

# new version
calc_ts <- function(EAP_triggered_observed, EAP_triggered_predicted_probs){
  library(verification)
  probs <- seq(0,1,by=0.01)
  POD_FAR_TS_list <- data.frame(pod_list = rep(NA, length(probs)),
                                far_list = rep(NA, length(probs)),
                                ts_list = rep(NA, length(probs)))
  for(i in 1:length(probs)){
    EAP_triggered <- EAP_triggered_predicted_probs >= probs[i]-0.01
    summary_stats <- table.stats(EAP_triggered_observed, EAP_triggered)
    POD_FAR_TS_list$ts_list[i] <- summary_stats$TS
    POD_FAR_TS_list$far_list[i] <- summary_stats$FAR
    POD_FAR_TS_list$pod_list[i] <- summary_stats$POD
  }
  return(POD_FAR_TS_list)
}

pod_far_ts_statistical <- calc_ts(EAP_triggered_obs, EAP_triggered_MODEL_probs)
pod_far_ts_glofas <- calc_ts(EAP_triggered_obs, EAP_triggered_GLOFAS_probs)
pod_far_ts_multi <- calc_ts(EAP_triggered_obs, EAP_triggered_multi_MODEL_least_squares_probs)
if(is_amazon){
  pod_far_ts_statistical_amazon <- ts_statistical
  pod_far_ts_glofas_amazon <- ts_glofas
  pod_far_ts_multi_amazon <- ts_multi
  save(pod_far_ts_statistical_amazon, pod_far_ts_glofas_amazon, pod_far_ts_multi_amazon, file = "pod_far_ts_data/amazon_pod_far_ts_data.RData" )
}
if(is_maranon){
  pod_far_ts_statistical_maranon <- pod_far_ts_statistical
  pod_far_ts_glofas_maranon <- pod_far_ts_glofas
  pod_far_ts_multi_maranon <- pod_far_ts_multi
  save(pod_far_ts_statistical_maranon, pod_far_ts_glofas_maranon, pod_far_ts_multi_maranon, file = "pod_far_ts_data/maranon_pod_far_ts_data.RData" )
}
if(is_piura){
  pod_far_ts_statistical_piura <- pod_far_ts_statistical
  pod_far_ts_glofas_piura <- pod_far_ts_glofas
  pod_far_ts_multi_piura <- pod_far_ts_multi
  save(pod_far_ts_statistical_piura, pod_far_ts_glofas_piura, pod_far_ts_multi_piura, file = "pod_far_ts_data/piura_pod_far_ts_data.RData" )
}


# PLOTTING

# New Way:
load("pod_far_ts_data/piura_pod_far_ts_data.RData")
load("pod_far_ts_data/maranon_pod_far_ts_data.RData")
probs <- c(0:100)

# dataframes for Threat Score
ts_plot_data_maranon <- data.frame(probs=probs,
                           statistical = pod_far_ts_statistical_maranon$ts_list,
                           glofas = pod_far_ts_glofas_maranon$ts_list,
                           multi = pod_far_ts_multi_maranon$ts_list)
ts_plot_data_piura <- data.frame(probs=probs,
                           statistical = pod_far_ts_statistical_piura$ts_list,
                           glofas = pod_far_ts_glofas_piura$ts_list,
                           multi = pod_far_ts_multi_piura$ts_list)
# dataframes for POD
pod_plot_data_maranon <- data.frame(probs=probs,
                           statistical = pod_far_ts_statistical_maranon$pod_list,
                           glofas = pod_far_ts_glofas_maranon$pod_list,
                           multi = pod_far_ts_multi_maranon$pod_list)
pod_plot_data_piura <- data.frame(probs=probs,
                           statistical = pod_far_ts_statistical_piura$pod_list,
                           glofas = pod_far_ts_glofas_piura$pod_list,
                           multi = pod_far_ts_multi_piura$pod_list)
# dataframes for FAR
far_plot_data_maranon <- data.frame(probs=probs,
                           statistical = pod_far_ts_statistical_maranon$far_list,
                           glofas = pod_far_ts_glofas_maranon$far_list,
                           multi = pod_far_ts_multi_maranon$far_list)
far_plot_data_piura <- data.frame(probs=probs,
                           statistical = pod_far_ts_statistical_piura$far_list,
                           glofas = pod_far_ts_glofas_piura$far_list,
                           multi = pod_far_ts_multi_piura$far_list)


#new way
library(ggplot2)
make_plot <- function(data, color_palette=cbbPalette, xlabel, annotation_text, x_annotation_alignment, show_legend=F){
  data_m <- reshape2::melt(data, id.var=colnames(data)[1])
  single_plot = ggplot(data_m, aes(x = probs, y = value, color=variable)) +
    geom_line(size=1) +
    # add legend and remove title
    scale_color_manual(labels=c("Statistical", "GloFAS", "Multi-model"),
                       values=color_palette[3:1]) +
    labs(color='') +
    theme(legend.title = element_blank()) +
    # axis labeling, etc
    xlab(xlabel) +
    ylab('') +
    annotate("text", x = x_annotation_alignment, y = 0.95, label = annotation_text, size=6) +
    ylim(0,1) +
    theme_bw() +
    theme(panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          axis.text=element_text(size=14, face="bold"),
          axis.title.x = element_text(size=16, face="bold"),
          legend.position = c(0.2, 0.3),
          legend.text=element_text(size=16),
          legend.key.size = unit(3,"line"))

    if(!show_legend){
      single_plot <- single_plot + theme(legend.position = "none")
    }
  return(single_plot)
}


# POD
maranon_pod <- make_plot(data=pod_plot_data_maranon,
                        xlabel = "",
                        annotation_text = "(a)",
                        x_annotation_alignment = 5,
                        show_legend = T)

piura_pod <- make_plot(data=pod_plot_data_piura,
                        xlabel = "",
                        annotation_text = "(b)",
                       x_annotation_alignment = 95)
# FAR
maranon_far <- make_plot(data=far_plot_data_maranon,
                        xlabel = "",
                        annotation_text = "(c)",
                        x_annotation_alignment = 5)

piura_far <- make_plot(data=far_plot_data_piura,
                        xlabel = "",
                        annotation_text = "(d)",
                        x_annotation_alignment = 95)
# TS
maranon_ts <- make_plot(data=ts_plot_data_maranon,
                        xlabel = "Mara\u00F1\u00F3n",
                        annotation_text = "(e)",
                        x_annotation_alignment = 5)

piura_ts <- make_plot(data=ts_plot_data_piura,
                        xlabel = "Piura",
                        annotation_text = "(f)",
                      x_annotation_alignment = 95)

library(gridExtra)
final_plot_ts <- grid.arrange(maranon_ts, piura_ts, bottom=textGrob("Forecast Probability Required to Trigger Early Action (%)", gp=gpar(fontsize=16,fontface="bold"), rot = 0), left=textGrob("    TS", gp=gpar(fontsize=16,fontface="bold"), rot = 90), nrow=1, ncol=2)

final_plot_far <- grid.arrange(maranon_far, piura_far, left=textGrob("    FAR", gp=gpar(fontsize=16,fontface="bold"), rot = 90),nrow=1, ncol=2)
final_plot_pod <- grid.arrange(maranon_pod, piura_pod, left=textGrob("    POD", gp=gpar(fontsize=16,fontface="bold"), rot = 90),nrow=1, ncol=2)

final_plot_all <- grid.arrange(final_plot_pod, final_plot_far, final_plot_ts, nrow=3, ncol=1)

ggsave("/Users/Colin/Box Sync/Research/manuscripts/Paper images and tables/ts_far_pod_maranon_piura_new.png", plot=final_plot_all, width=375, height=450, units="mm")
```



## GloFAS-only Forecast verification

Compute accuracy (fraction of predictions in correct category) and contingency table
```{r}
n_cat <- if(use_two_category==TRUE){
  2
}else{
  3
}

source("/Users/Colin/Box Sync/Research/SST_Streamflow/compute_accuracy_contingency.R")

acc_cont <- compute_accuracy_contingency(obs_sf=obs_sf,
                                         predicted_sf=colMeans(all_ensembles, na.rm=TRUE),
                                         use_equal_sized_categories=use_equal_sized_categories,
                                         n_categories=n_cat, 
                                         high_threshold=high_threshold, 
                                         medium_threshold=medium_threshold)

accuracy <- unlist(acc_cont[1])
contingency_table <- acc_cont[2]

accuracy
contingency_table
```

Pearson correlation
```{r}
cor.test(colMeans(all_ensembles, na.rm=TRUE), obs_sf, method="pearson")
```


put data in correct form for RPSS
```{r}
source("/Users/Colin/Box Sync/Research/SST_Streamflow/get_categorical_probs.R")
RPSS_obs_pred <- get_categorical_probs(all_ensembles = all_ensembles[1:25,], 
                              n_categories=n_cat, 
                              use_equal_sized_categories = use_equal_sized_categories, 
                              high_threshold = high_threshold,
                              medium_threshold = medium_threshold,
                              obs_sf=obs_sf)

observed_probs <- unlist(RPSS_obs_pred[1])
predicted_probs <- unlist(RPSS_obs_pred[2])
```

Compute RPSS
```{r}

source("/Users/Colin/Box Sync/Research/SST_Streamflow/compute_RPSS.R")
rpss <- compute_RPSS(pred=predicted_probs, 
                      obs=observed_probs, 
                      use_equal_sized_categories = use_equal_sized_categories, 
                      n_categories=n_cat, 
                      climatology_high = (1-high_low_cutoff),
                      climatology_med = NA,
                      climatology_low = high_low_cutoff,
                      n_years=length(obs_sf)
                     )
rpss
```







ensemble LEAST SQUARES statistics
```{r}
# accuracy
multi_MODEL_least_squares[2]

# skill matrix
multi_MODEL_least_squares[3]

# RPSS
multi_MODEL_least_squares[4]

# pearson corr
multi_MODEL_least_squares[5]
```

ensemble EQUAL WEIGHT statistics
```{r}
# accuracy
multi_MODEL_equal_weight[2]

# skill matrix
multi_MODEL_equal_weight[3]

# RPSS
multi_MODEL_equal_weight[4]

# pearson corr
multi_MODEL_equal_weight[5]
```

