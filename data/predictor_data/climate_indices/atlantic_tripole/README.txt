1st EOF of SST 10N-70N, 0-80W
GISST 1948-1949
Reconstructed Reynolds 1950-1981
OI 1982-present
Deser, Clara, Michael S. Timlin, 1997: Atmosphere-Ocean Interaction on Weekly Timescales in the North Atlantic and Pacific. Journal of Climate: Vol. 10, No. 3, pp.393-408. 

07/17/19 Colin: accessed via 
https://www.esrl.noaa.gov/psd/data/climateindices/list/#Atlantictripole
https://www.esrl.noaa.gov/psd/data/correlation/atltri.data
