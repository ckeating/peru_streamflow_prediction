NP Index is the area-weighted sea level pressure over the region 30N-65N, 160E-140W.

Time Interval: Monthly
Time Coverage: 1899 to Mar 2019
Update Status: Periodically updated

Get Data:

    NP: Standard PSD Format (What is standard format?)
    NP: Original

Source:
Data is from NCAR/CGD at https://climatedataguide.ucar.edu/climate-data/north-pacific-np-index-trenberth-and-hurrell-monthly-and-winter.
References:

    Trenberth and Hurrell (1994), Climate Dynamics 9:303-319 


May 14, 2019 Colin: Accessed via https://www.esrl.noaa.gov/psd/gcos_wgsp/Timeseries/NP/