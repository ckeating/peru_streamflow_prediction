---
title: "R Notebook"
output: html_notebook
---

temporary code for adding predictors for a different season: here, for SON
```{r}
  # obtain SSTs for southern coast of Chile
  source("/Users/Colin/Box Sync/Research/SST_Streamflow/SST_monthly_means.R")
  SST_monthly_means_chile <- SST_monthly_means(-45, -38, 270, 285, 1985, 2017)

  # obtain SSTs for east-central Atlantic
  SST_monthly_means_ec_atl <- SST_monthly_means(15, 25, 320, 340, 1985, 2017)

  # obtain SSTs for Nova Scotia
  SST_monthly_means_nova <- SST_monthly_means(38, 45, 300, 306, 1985, 2017)

  # instantiate vector to hold seasonal mean SST values
  SST_chile <- rep(0, length(SST_monthly_means_chile)/12-1)
  SST_ec_atl <- rep(0, length(SST_monthly_means_chile)/12-1)
  SST_nova <- rep(0, length(SST_monthly_means_chile)/12-1)
  # nino34_abs <- rep(0, length(SST_monthly_means_chile)/12-1)
  # get SST val for each month and add mean value to vector
  for (i in 1:(length(SST_monthly_means_chile)/12-1)) {
    # chile
    O_chile <- SST_monthly_means_chile[(i*12)-2]
    N_chile <- SST_monthly_means_chile[(i*12)-1]
    S_chile <- SST_monthly_means_chile[(i*12)-3]
    SST_chile[i] <- mean(c(O_chile, N_chile, S_chile))

    # east central Atlantic
    S_ecatl <- SST_monthly_means_ec_atl[(i*12)-3]
    O_ecatl <- SST_monthly_means_ec_atl[(i*12)-2]
    N_ecatl <- SST_monthly_means_ec_atl[(i*12)-1]
    SST_ec_atl[i] <- mean(c(S_ecatl, O_ecatl, N_ecatl))

    # nova
    S_nova <- SST_monthly_means_nova[(i*12)-3]
    O_nova <- SST_monthly_means_nova[(i*12)-2]
    N_nova <- SST_monthly_means_nova[(i*12)-1]
    SST_nova[i] <- mean(c(S_nova, O_nova, N_nova))
  }
  
  saveRDS(SST_chile, file = "/Users/Colin/Box Sync/Research/SST_Streamflow/predictor_data/SST_chile_SON.rds")
  saveRDS(SST_ec_atl, file = "/Users/Colin/Box Sync/Research/SST_Streamflow/predictor_data/SST_ec_atl_SON.rds")
  saveRDS(SST_nova, file = "/Users/Colin/Box Sync/Research/SST_Streamflow/predictor_data/SST_nova_SON.rds")
```


